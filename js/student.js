/*
// function searchStudent()
// {
	// var sName = document.getElementsByName("student-name")[0].value;
	// var sRoll = document.getElementsByName("student-roll")[0].value;
	// console.log(typeof(sName));
	// var sName=string(Name);
	// if(sName != '' && !isNaN(sRoll)){
		// console.log('1st Name: '+name+' and Roll: '+roll);
		// console.log(student);
		
		// 	Searching the data
		// for (var r = 0; r < student.length; r++) {
			// console.log(student[r]);
			// console.log(sName);
			// console.log(sRoll);
			// if(student[r].name === sName && student[r].roll === sRoll){
				// console.log('Conditions matched');
			// }	
		// }
		
		// var result = student.filter(obj => {
		  // return obj.name === sName
		// })
		// console.log(result);
		
		//	Searching the data ends
	// }
	// else{
		// console.log('I am EMPTY!!!!');
	// }
// }
*/


/*	calling the function on page load */
// setTimeout(function() { studentTable(); }, 100);
window.onload = function() {
  studentTableWA();
  // studentTable();
};
/*	calling the function on page load ends*/

// Sorting by roll
function sort(){
    for(var i = 0; i < student.length; i++){
        for(var j = i+1; j < student.length; j++){
            if( student[i].roll > student[j].roll)
                [student[i],student[j]]=[student[j],student[i]];
        }
    }
}
//Sorting by roll ends

/*
function validate(name,roll) {
    var error = 0;
    
    if (name == null || name == "") {
        // alert("Name must be filled out");
        error++;
		document.getElementById('name_error').innerHTML = 'Name must be filled out';
	}
	if(name.length < 5){
		document.getElementById('name_error').innerHTML = 'Name must be more then 5 Letters';
	}
	
    if (roll == null || roll == "") {
        // alert("Roll no must be filled out");
        error++;
        document.getElementById('roll_error').innerHTML = 'Roll no must be filled out';
    }

    if(error>0) {
        return false;
    }
    return true;
}
*/

/* Rendering table by the id of a div*/
function studentTableWA(){
	
	// var newData=
	// pubsub.subscribe('studentChange', student);
	// console.log('Subscriber ',newData);
	
	// console.log("subscriber ",pubsub.subscribe('studentChange', student));
	sort();
var table = '<div class="student-data">';
// table = '';
table+="<table>";
table+="<thead>";
for (var r = 0; r < 1; r++) {
	table+="<tr>";
	var objKey = Object.keys(student[r]);
	//console.log(objKey[1]);
	for(var c=0; c<objKey.length; c++){
		//console.log(objKey[c]);
		var key=objKey[c];
		//console.log(key);
		table+='<th>'+key+'</th>';
	}
	table+='<th>Action</th>';
	table+="</tr>";
}
table+="</thead>";
table+="<tbody>";
	//var con = student.length;
	//console.log(con);
	var id=new Array();
for (var r = 0; r < student.length; r++) {
	table+="<tr>";
	var objKey = Object.keys(student[r]);
	//console.log(objKey.length);
	for(var c=0; c<objKey.length; c++){
		//console.log(objKey[c]);
		var key=objKey[c];
		//console.log(key);
		table+='<td >'+ student[r][key]+'</td>';
		// var rIndex = this.parentElement.rowIndex;
	}
	id[r]=r;
	//console.log(id[r]);
	table+='<td class="action"><button class="edit popupModal" onclick="editStudent('+id[r]+')"><i class="fas fa-edit"></i></i></button><button class="delete" onclick="deleteStudent('+id[r]+')"><i class="fas fa-trash-alt"></i></button></td>';
	
	table+="</tr>";
}
table+="</tbody>";
table+="<tfoot>";
table+="</tfoot>";
table+="</table>";
table+="</div>";
document.getElementById("studentWA").innerHTML = table;
pubsub.unsubscribe('studentChange', student);
}
/* Rendering table by the id of a div*/


function studentTable(){
	
	// var sub = pubsub.subscriber('studentChange', student);
	// console.log(sub);
	
	
	 pubsub.subscribe('studentChange', student);
	
	
	sort();
var table = '<div class="student-data">';
// table = '';
table+="<table>";
table+="<thead>";
for (var r = 0; r < 1; r++) {
	table+="<tr>";
	var objKey = Object.keys(student[r]);
	//console.log(objKey[1]);
	for(var c=0; c<objKey.length; c++){
		//console.log(objKey[c]);
		var key=objKey[c];
		//console.log(key);
		table+='<th>'+key+'</th>';
	}
	//table+='<th>Action</th>';
	table+="</tr>";
}
table+="</thead>";
table+="<tbody>";
	//var con = student.length;
	//console.log(con);
	var id=new Array();
for (var r = 0; r < student.length; r++) {
	table+="<tr>";
	var objKey = Object.keys(student[r]);
	//console.log(objKey.length);
	for(var c=0; c<objKey.length; c++){
		//console.log(objKey[c]);
		var key=objKey[c];
		//console.log(key);
		table+='<td >'+ student[r][key]+'</td>';
		// var rIndex = this.parentElement.rowIndex;
	}
	//id[r]=r;
	//console.log(id[r]);
	//table+='<td class="action"><button class="edit popupModal" onclick="editStudent('+id[r]+')"><i class="fas fa-edit"></i></i></button><button class="delete" onclick="deleteStudent('+id[r]+')"><i class="fas fa-trash-alt"></i></button></td>';
	
	table+="</tr>";
}
table+="</tbody>";
table+="<tfoot>";
table+="</tfoot>";
table+="</table>";
table+="</div>";
document.getElementById("student").innerHTML = table;
pubsub.unsubscribe('studentChange', student);
}





/* Attributes & Functions for modal	*/
var modal = document.querySelector(".modal");
var trigger = document.querySelector(".popupModal");
var closeButton = document.querySelector(".close-button");
window.addEventListener("click", windowOnClick);

function toggleModal() {
    modal.classList.toggle("show-modal");
}
function closeModal() {
    modal.classList.toggle("show-modal");
}
function windowOnClick(event) {
    if (event.target === modal) {
        toggleModal();
    }
}
/* Attributes & Functions for modal Ends	*/


/*	Add New Student	*/
function addStudent(){
	
	toggleModal();
	
	editModal='<div class="modal-content">';
	editModal+='<span class="close-button" onclick="closeModal()">×</span>';
	editModal+='<h2>Add Student Profile</h2>'
	editModal+=		'<div class="modal-data">';
	editModal+=			'<div class="data-items">';
	editModal+=				'<label for="new-student-name">Student Name: </label>';
	editModal+=				'<input class="input" type="text" name="new-student-name" value="" placeholder="Enter Student Name">';
	// editModal+=				'<span class="danger"></span>';
	editModal+=			'</div>';
	editModal+=			'<div class="data-items">';
	editModal+=				'<label for="new-student-roll">Student Roll: </label>';
	editModal+=				'<input class="input" type="number" name="new-student-roll" value="" placeholder="Enter Student Roll">';
	editModal+=			'</div>';
	editModal+=			'<div class="data-items">';
	editModal+=				'<button class="btn-edit" onclick="storeStudent()">Save</button>';
	// editModal+=				'<button class="btn-delete" onclick="closeModal()">Cancle</button>';
	editModal+=			'</div>';
	editModal+=		'</div>';
	editModal+='</div>';
	
	document.querySelector(".modal").innerHTML = editModal;
}
function storeStudent() {
	
	// Catching the data via name element
    var sName = document.getElementsByName("new-student-name")[0].value;
	var sRoll = document.getElementsByName("new-student-roll")[0].value;
	//console.log(sName);
	//student[id].name = sName;
	//student[id].roll = sRoll;
	
	// var validateData = validate(sName,sRoll);
	
	
	
	// Create new object with the data
	var newStudent = {
		name: sName,
		roll: parseInt(sRoll)
	}
	
	// Pushing the data to existing object
	student.push(newStudent);
	// console.log(student);
	
	// Store updated data to local storage
	// window.localStorage.setItem('studentChange', JSON.stringify(student));
	
	// Store updated data to session storage
	// window.sessionStorage.setItem('studentChange', JSON.stringify(student));
	
	// Close the modal
	closeModal();
	
	
	pubsub.publish('studentChange', student);
	
	
	// Viewing the table with updated student object
	studentTable();
	studentTableWA();
	
	
	
}
/*	Add New Student Ends	*/



/*edit information*/
function editStudent(id){
	//console.log(id);
	toggleModal();
	
	editModal='<div class="modal-content">';
	editModal+='<span class="close-button" onclick="closeModal()">×</span>';
	editModal+='<h2>Edit Student Profile</h2>'
	editModal+=		'<div class="modal-data">';
	editModal+=			'<div class="data-items">';
	editModal+=				'<label for="edit-student-name">Student Name: </label>';
	editModal+=				'<input class="input" type="text" name="edit-student-name" value="'+student[id].name+'">';
	editModal+=			'</div>';
	editModal+=			'<div class="data-items">';
	editModal+=				'<label for="edit-student-roll">Student Roll: </label>';
	editModal+=				'<input class="input" type="number" name="edit-student-roll" value="'+student[id].roll+'">';
	editModal+=			'</div>';
	editModal+=			'<div class="data-items">';
	editModal+=				'<button class="btn-edit" onclick="updateStudent('+id+')">Update</button>';
	// editModal+=				'<button class="btn-delete" onclick="closeModal()">Cancle</button>';
	editModal+=			'</div>';
	editModal+=		'</div>';
	editModal+='</div>';
	document.querySelector(".modal").innerHTML = editModal;
}
function updateStudent(id) {
	
	// Catching the data via name element
    var sName = document.getElementsByName("edit-student-name")[0].value;
	var sRoll = document.getElementsByName("edit-student-roll")[0].value;
	//console.log(sName);
	//student[id].name = sName;
	//student[id].roll = sRoll;
	
	//	update the data and assign the value
	Object.assign(student[id], {
		name: sName,
		roll: parseInt(sRoll)
		}
	)
	// console.log(student);
	
	// Store updated data to local storage
	// window.localStorage.setItem('studentChange', JSON.stringify(student));
	
	// Store updated data to session storage
	// window.sessionStorage.setItem('studentChange', JSON.stringify(student));
	
	// Close the modal
	closeModal();
	
	// Viewing the table with updated Student object
	studentTable();
	studentTableWA();
	
	pubsub.publish('studentChange', student);
	
	
	
	
}
/*edit information ends*/



/*Deleting information*/
function deleteStudent(id) {
	
	student.splice(id,1);
	
	// Store updated data to local storage
	// window.localStorage.setItem('studentChange', JSON.stringify(student));
	
	// Store updated data to session storage
	// window.sessionStorage.setItem('studentChange', JSON.stringify(student));
	
	//Viewing table after deleting a student object
	studentTable();
	studentTableWA();
	
	pubsub.publish('studentChange', student);
	
	// console.log(id);
	// console.log(student);
	
	
}
/*Deleting information ends*/
